package com.example.aluno.calculadoragorjeta

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import java.lang.Double


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var valorEntrada = txtValor.text


        seekBar.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            internal var progresso = 0

            override fun onProgressChanged(seekBar: SeekBar, progresValue: Int, fromUser: Boolean) {
                progresso = progresValue

                tvProgresso.setText("" + progresso + "%")

                if (!valorEntrada.isEmpty()){
                    val somaGorjeta =  Double.parseDouble(valorEntrada.toString()) * progresso / 100
                    val somaTotal = somaGorjeta + Double.parseDouble(valorEntrada.toString())
                    tvTotalGorjeta.setText("R$ " + somaGorjeta.toString())
                    tvTotalValor.setText("R$ " + somaTotal.toString())
                }else{
                    tvTotalGorjeta.setText("")
                    tvTotalValor.setText("")
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}

            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })


    }

}
